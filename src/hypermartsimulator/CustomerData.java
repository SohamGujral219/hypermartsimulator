package hypermartsimulator;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class is used to store information which is consistent for all
 * customers.
 *
 * @author Soham Gujral
 */
public class CustomerData {

    private static int pointerForCustomerArray;
    //THE LIST CONTAINING ALL THE POSSIBLE CUSTOMERS
    private ObservableList<Customer> customers;
    //THE INITIAL NUMBER OF CUSTOMERS TO START WITH
   
    private double avgInterestsPerCustomer;

    private int numberOfCustomerToRun;
    
    private int minInterestTimeForCustomer;
    private int maxInterestTimeForCustomer;

    public CustomerData() {
        pointerForCustomerArray = 0;
        this.customers = FXCollections.observableArrayList();
    }

    /**
     * THIS METHOD RETURN A COMPLETE LIST OF CUSTOMERS
     *
     * @return ArrayList of customers
     */
    public ObservableList<Customer> getCustomersObservableList() {
        return customers;
    }

    /**
     * THIS METHOD ADDS A CUSTOMER TO THE LIST
     *
     * @param customer THE NEW CUSTOMER TO BE ADDED
     */
    public void addCustomer(Customer customer) {
        this.customers.add(customer);
    }

    /**
     * THIS METHOD CLEARS ALL THE CUSTOMERS
     */
    public void Clear() {
        this.customers = FXCollections.observableArrayList();
    }

    //ALL THE GETTERS AND SETTERS
    public void setCustomers(ObservableList<Customer> customers) {
        this.customers = customers;
    }




    public double getAvgInterestsPerCustomer() {
        return avgInterestsPerCustomer;
    }

    public void setAvgInterestsPerCustomer(double avgInterestsPerCustomer) {
        this.avgInterestsPerCustomer = avgInterestsPerCustomer;
    }

    public int getNumberOfCustomerToRun() {
        return numberOfCustomerToRun;
    }

    public void setNumberOfCustomerToRun(int numberOfCustomerToRun) {
        this.numberOfCustomerToRun = numberOfCustomerToRun;
    }

    public static int getPointerForCustomerArray() {
        return pointerForCustomerArray;
    }

    public static void setPointerForCustomerArray(int pointerForCustomerArray) {
        CustomerData.pointerForCustomerArray = pointerForCustomerArray;
    }

    public int getMinInterestTimeForCustomer() {
        return minInterestTimeForCustomer;
    }

    public void setMinInterestTimeForCustomer(int minInterestTimeForCustomer) {
        this.minInterestTimeForCustomer = minInterestTimeForCustomer;
    }

    public int getMaxInterestTimeForCustomer() {
        return maxInterestTimeForCustomer;
    }

    public void setMaxInterestTimeForCustomer(int maxInterestTimeForCustomer) {
        this.maxInterestTimeForCustomer = maxInterestTimeForCustomer;
    }

    
}
