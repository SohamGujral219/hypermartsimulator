package hypermartsimulator;

import java.util.ArrayList;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Soham
 */
public class Store {

    ObservableList<Customer> customers;
    private IntegerProperty uniqueStoreId;
    private final StringProperty storeName;
    private final IntegerProperty numberOfSectionsInStore;
    private final IntegerProperty numberOfCustomerForSimulation;

    private int xVariables;
    private int yVariables;

    public Store(String storeName, int numberOfSectionsInStore, int numberOfCustomerForSimulation) {
        this.uniqueStoreId = new SimpleIntegerProperty();
        this.storeName = new SimpleStringProperty(storeName);
        this.numberOfSectionsInStore = new SimpleIntegerProperty(numberOfSectionsInStore);
        this.numberOfCustomerForSimulation = new SimpleIntegerProperty(numberOfCustomerForSimulation);
        customers = FXCollections.observableArrayList();
        this.calculateXYvariables();

    }

    public ObservableList<Customer> getCustomers() {
        return customers;
    }

    public IntegerProperty uniqueStoreIdProperty() {
        return uniqueStoreId;
    }

    public StringProperty storeNameProperty() {
        return storeName;
    }

    public IntegerProperty numberOfSectionsInStoreProperty() {
        return numberOfSectionsInStore;
    }

    public IntegerProperty numberOfCustomerForSimulationProperty() {
        return numberOfCustomerForSimulation;
    }

    public int getNumberOfSectionsInStore() {
        return numberOfSectionsInStore.get();
    }

    public void setNumberOfSectionsInStore(int numberOfSectionsInStore) {
        this.numberOfSectionsInStore.set(numberOfSectionsInStore);
    }

    public int getNumberOfCustomerForSimulation() {
        return numberOfCustomerForSimulation.get();
    }

    public void setNumberOfCustomerForSimulation(int numberOfCustomerForSimulation) {
        this.numberOfCustomerForSimulation.set(numberOfCustomerForSimulation);
    }

    public String getStoreName() {
        return storeName.get();
    }

    public void setStoreName(String storeName) {
        this.storeName.set(storeName);
    }

    public int getUniqueStoreId() {
        return uniqueStoreId.get();
    }

    public void setUniqueStoreId(int uniqueStoreId) {
        this.uniqueStoreId.set(uniqueStoreId);
    }

    @Override
    public String toString() {
        return "Store name = " + storeName + "\nNumber Of Sections In Store = " + numberOfSectionsInStore + "\nNumber Of Customer For Simulation = " + numberOfCustomerForSimulation + "\nAverage Customer Intrest Time = ";
    }

    private void calculateXYvariables() {
        switch (this.numberOfSectionsInStore.get()) {
            case 1:
                this.xVariables = 1;
                this.yVariables = 1;
                break;
            case 2:
                this.xVariables = 2;
                this.yVariables = 1;
                break;
            case 3:
                this.xVariables = 3;
                this.yVariables = 1;
                break;
            case 4:
                this.xVariables = 3;
                this.yVariables = 1;
                break;
            case 5:
                this.xVariables = 3;
                this.yVariables = 2;
                break;
            case 6:
                this.xVariables = 3;
                this.yVariables = 2;
                break;
            case 7:
                this.xVariables = 4;
                this.yVariables = 2;
                break;
            case 8:
                this.xVariables = 4;
                this.yVariables = 2;
                break;
            case 9:
                this.xVariables = 3;
                this.yVariables = 3;
                break;
            case 10:
                this.xVariables = 4;
                this.yVariables = 3;
                break;
            case 11:
                this.xVariables = 4;
                this.yVariables = 3;
                break;
            case 12:
                this.xVariables = 4;
                this.yVariables = 3;
                break;
            case 13:
                this.xVariables = 7;
                this.yVariables = 2;
                break;
            case 14:
                this.xVariables = 7;
                this.yVariables = 2;
                break;
            default:
                break;
        }

    }

    public int getxVariables() {
        return xVariables;
    }

    public void setxVariables(int xVariables) {
        this.xVariables = xVariables;
    }

    public int getyVariables() {
        return yVariables;
    }

    public void setyVariables(int yVariables) {
        this.yVariables = yVariables;
    }

}
