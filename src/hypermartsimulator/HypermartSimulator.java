/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hypermartsimulator;

import hypermartsimulator.fileIO.CSVUtils;
import hypermartsimulator.fileIO.DataIO;
import hypermartsimulator.fileIO.EmailSender;
import hypermartsimulator.fileIO.SmsSender;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.application.Application;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 *
 * @author Soham
 */
public class HypermartSimulator extends Application {

    DataIO dataManager = new DataIO();
    CustomerData customerDataManager = new CustomerData();
    StoreData appDataManager = new StoreData();

    @Override
    public void start(Stage primaryStage) {
        //new SmsSender("Hello", "9717616965");

        TabPane tabPane = new TabPane();

        Tab parameterTab = new Tab();
        parameterTab.setText("Parameters");

        GridPane paremeterTabGrid = new GridPane();
        paremeterTabGrid.setPadding(new Insets(15, 15, 15, 15));
        paremeterTabGrid.setHgap(10);
        paremeterTabGrid.setVgap(10);

        Label customerInputJsonLabel = new Label("Enter path for JSON file to import the customers");
        Label exportDirLabel = new Label("Enter path for exporting the results");

        Label numberOfInterestLabel = new Label("Enter the range of the number of interests a customer can have");
        Label timeSpentInAreaLabel = new Label("Enter the range of the number of seconds a customer spends in his area of interest");

        Label smsThresholdLabel = new Label("Enter the threshold for sending the interest SMS\\Email");

        TextField customerInputJsonTextFeild = new TextField();
        TextField exportDirTextFeild = new TextField();

        TextField minInterestTextFeild = new TextField();
        TextField maxInterestTextFeild = new TextField();

        TextField minTimeSpentTextFeild = new TextField();
        TextField maxTimeSpentTextFeild = new TextField();

        TextField smsThresholdTextFeild = new TextField();

        Button loadCustomerButton = new Button();
        loadCustomerButton.setText("Load Customers");

        Button runSimulationButton = new Button();
        runSimulationButton.setText("Run Simulation");

        paremeterTabGrid.add(customerInputJsonLabel, 0, 0);
        paremeterTabGrid.add(customerInputJsonTextFeild, 1, 0);
        paremeterTabGrid.add(loadCustomerButton, 2, 0);
        paremeterTabGrid.add(exportDirLabel, 0, 1);
        paremeterTabGrid.add(exportDirTextFeild, 1, 1);

        paremeterTabGrid.add(numberOfInterestLabel, 0, 2);
        paremeterTabGrid.add(minInterestTextFeild, 1, 2);
        paremeterTabGrid.add(maxInterestTextFeild, 2, 2);

        paremeterTabGrid.add(timeSpentInAreaLabel, 0, 3);
        paremeterTabGrid.add(minTimeSpentTextFeild, 1, 3);
        paremeterTabGrid.add(maxTimeSpentTextFeild, 2, 3);

        paremeterTabGrid.add(smsThresholdLabel, 0, 4);
        paremeterTabGrid.add(smsThresholdTextFeild, 1, 4);

        paremeterTabGrid.add(runSimulationButton, 0, 5);

        parameterTab.setContent(paremeterTabGrid);
        TableView<Customer> table = new TableView<>();

        TableColumn<Customer, String> firstNameCol = new TableColumn("First Name");
        TableColumn<Customer, String> lastNameCol = new TableColumn("Last Name");
        TableColumn<Customer, String> emailCol = new TableColumn("Email");
        TableColumn<Customer, String> macCol = new TableColumn("Mac Address");
        TableColumn<Customer, String> mobileCol = new TableColumn("Mobile Number");
        TableColumn<Customer, Integer> serialCol = new TableColumn("Serial");
        //table.setEditable(true);

        firstNameCol.setCellValueFactory(
                new PropertyValueFactory<>("firstName")
        );
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        emailCol.setCellValueFactory(
                new PropertyValueFactory<>("emailAddress")
        );
        macCol.setCellValueFactory(
                new PropertyValueFactory<>("macAddress")
        );
        mobileCol.setCellValueFactory(
                new PropertyValueFactory<>("mobileNumber")
        );
        serialCol.setCellValueFactory(
                new PropertyValueFactory<>("serial")
        );

        table.setItems(customerDataManager.getCustomersObservableList());

        table.getColumns().add(serialCol);
        table.getColumns().add(firstNameCol);
        table.getColumns().add(lastNameCol);
        table.getColumns().add(emailCol);
        table.getColumns().add(macCol);
        table.getColumns().add(mobileCol);

        loadCustomerButton.setOnAction((ActionEvent event) -> {
            this.loadData(customerInputJsonTextFeild.getText());
            table.setItems(customerDataManager.getCustomersObservableList());

            table.refresh();
        });

        runSimulationButton.setOnAction((ActionEvent event) -> {

            int minNumberInterest = Integer.parseInt(minInterestTextFeild.getText());
            int maxNumberInterest = Integer.parseInt(maxInterestTextFeild.getText());
            this.setCustomerNumberOfInterest(minNumberInterest, maxNumberInterest);

            customerDataManager.setMinInterestTimeForCustomer(Integer.parseInt(minTimeSpentTextFeild.getText()));
            customerDataManager.setMaxInterestTimeForCustomer(Integer.parseInt(maxTimeSpentTextFeild.getText()));
            this.setInitialCustomerInterestTime();

            dataManager.setSmsThresholdTime(Integer.parseInt(smsThresholdTextFeild.getText()));
            this.simulate();

        });
        Tab customersTab = new Tab();
        customersTab.setText("Customers");
        customersTab.setContent(table);

        Tab storesTab = new Tab();
        storesTab.setText("Stores");
        GridPane storeGridPane = new GridPane();

        Label storeNameLabel = new Label("Enter the Store Name: ");
        Label numberOfStoreSectionLabel = new Label("Enter the number of sections in the Store: ");
        Label numberOfCustomerLabel = new Label("Enter the number of customers to simulate: ");

        TextField storeNameTextFeild = new TextField();
        TextField storeSectionTextFeild = new TextField();
        TextField numberOfCustomerTextFeild = new TextField();

        Button addStoreButton = new Button();
        addStoreButton.setText("Add Store");

        storeGridPane.setPadding(new Insets(15));
        storeGridPane.setVgap(15);
        storeGridPane.setHgap(15);
        storeGridPane.add(storeNameLabel, 0, 0);
        storeGridPane.add(storeNameTextFeild, 1, 0);
        storeGridPane.add(numberOfStoreSectionLabel, 0, 1);
        storeGridPane.add(storeSectionTextFeild, 1, 1);
        storeGridPane.add(numberOfCustomerLabel, 0, 2);
        storeGridPane.add(numberOfCustomerTextFeild, 1, 2);
        storeGridPane.add(addStoreButton, 0, 3);

        TableView<Store> storeTable = new TableView();

        TableColumn<Store, Integer> storeNumberCol = new TableColumn<>("Store ID");
        TableColumn<Store, String> storeNameCol = new TableColumn<>("Store Name");
        TableColumn<Store, Integer> storeSectionCol = new TableColumn<>("Number of Sections");
        TableColumn<Store, Integer> storeCustomerCol = new TableColumn<>("Number of Customers");

        storeNumberCol.setCellValueFactory(
                new PropertyValueFactory<>("uniqueStoreId")
        );
        storeNameCol.setCellValueFactory(
                new PropertyValueFactory<>("storeName")
        );
        storeSectionCol.setCellValueFactory(
                new PropertyValueFactory<>("numberOfSectionsInStore")
        );
        storeCustomerCol.setCellValueFactory(
                new PropertyValueFactory<>("numberOfCustomerForSimulation")
        );
        addStoreButton.setOnAction((ActionEvent event) -> {
            String storeName = storeNameTextFeild.getText();
            int numberOfSections = Integer.parseInt(storeSectionTextFeild.getText());
            int initialCustomers = Integer.parseInt(numberOfCustomerTextFeild.getText());
            this.addStore(storeName, numberOfSections, initialCustomers);
            storeTable.setItems(appDataManager.getStoresList());

        });
        storeTable.getColumns().addAll(storeNumberCol, storeNameCol, storeSectionCol, storeCustomerCol);
        storeTable.setItems(appDataManager.getStoresList());

        VBox storeAddBox = new VBox();
        storeAddBox.getChildren().add(storeGridPane);
        storeAddBox.getChildren().add(storeTable);

        storesTab.setContent(storeAddBox);

        tabPane.getTabs().add(parameterTab);
        tabPane.getTabs().add(customersTab);
        tabPane.getTabs().add(storesTab);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        Scene scene = new Scene(tabPane, 800, 800);

        primaryStage.setTitle("Hypermart Simulator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public void loadData(String fileInput) {
        try {
            dataManager.loadData(customerDataManager, fileInput);
            System.out.println("Data Loaded");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addStore(String storeName, int numberOfSections, int initialCustomers) {
        Store newStore = new Store(storeName, numberOfSections, initialCustomers);
        newStore.setUniqueStoreId(appDataManager.getUniqueStoreID());
        int getInitialArrayPointer = CustomerData.getPointerForCustomerArray();
        for (int i = getInitialArrayPointer; i < (getInitialArrayPointer + initialCustomers); i++) {
            if (CustomerData.getPointerForCustomerArray() == customerDataManager.getCustomersObservableList().size()) {
                System.out.println("Not enough customers");
                break;
            }
            newStore.getCustomers().add(customerDataManager.getCustomersObservableList().get(i));
            CustomerData.setPointerForCustomerArray(CustomerData.getPointerForCustomerArray() + 1);
        }
        appDataManager.getStoresList().add(newStore);
        appDataManager.incrementStoreID();
        System.out.println("Customers added" + newStore.getCustomers().size());

    }

    public void generateInitialTargetTEMP(int storeNum, int xVar, int yVar) {
        int arr[] = {30, 60, 90, 120, 150, 180, 210, 240, 270, 300};
        for (int i = 0; i < appDataManager.getStoresList().get(storeNum).getCustomers().size(); i++) {
            int xPos = (int) (Math.random() * xVar);
            int yPos = (int) (Math.random() * yVar);
            appDataManager.getStoresList().get(storeNum).getCustomers().get(i).setxCoordinateTarget(arr[xPos]);
            appDataManager.getStoresList().get(storeNum).getCustomers().get(i).setyCoordinateTarget(arr[yPos]);
        }
    }

    public void move(Customer cust, int storeNum) {
        double oldX = cust.getxCoordinate();
        double oldY = cust.getyCoordinate();

        double newX = oldX;
        double newY = oldY;

        //This can be either his interest or checkout counter
        double targetInterestX = cust.getxCoordinateTarget();
        double targetInterestY = cust.getyCoordinateTarget();

        //has he reached to the correct X Axis?
        boolean placeX = false;

        //has he reached to the correct Y Axis?
        boolean placeY = false;

        //SENDING THE CUSTOMER TOWARDS HIS TAGERT X AXIS
        if (oldX < targetInterestX) {
            //move him right
            newX = ++oldX;
        } else if (oldX > targetInterestX) {
            //move him left
            newX = --oldX;
        } else {
            //he has reached the correct X Axis
            placeX = true;
        }

        //SENDING THE CUSTOMER TOWARDS HIS CORRECT Y AXIS
        if (oldY < targetInterestY) {
            //MOVE HIM UP
            newY = ++oldY;
        } else if (oldY > targetInterestY) {
            //MOVE HIM DOWN
            newY = --oldY;
        } else {
            //HE HAS REACEHD THE CORRECT Y AXIS
            placeY = true;

        }
        //IF HE HAS REACHED THE TARGET LOCATION, THEN LETS PLAN HIS NEXT TARGET
        if (placeX && placeY) {

            cust.increamentShooter();
            if (cust.getShooterCounterSMS() >= dataManager.getSmsThresholdTime() && !cust.isSmsSent()) {
                String textToSend = "Hello " + cust.getFirstName() + " " + cust.getLastName() + "! You are currently standing at " + cust.getxCoordinate() + "," + cust.getyCoordinate() + "\nYou are currently in " + appDataManager.getStoresList().get(storeNum).getStoreName() + ". \nYour mobile number is " + cust.getMobileNumber().get() + "\nYour mac address is " + cust.getMacAddress().get();
                new EmailSender(cust.getFirstName(), textToSend, cust.getEmailAddress());
//new SmsSender(cust.getFirstName(), cust.getMobileNumber().get());
                cust.setSmsSent(true);
            }
            //IF HE HAS NOT EXAUSTED HIS TIME SINCE HE WAS SUPPOSED TO BE STATIONARY
            if (cust.getTimeSinceStationary() >= 2) {
                //REDUCE HIS STATIONARY TIME BY 1
                cust.setTimeSinceStationary(cust.getTimeSinceStationary() - 1);
            } else {
                //CUSTOMER HAS EXAUSTED HIS TIME AT INTREST SPOT AND ALSO HIS NUMBER FOF INTRESTS
                //HE WILL START EXITING THE STORE NOW
                if (cust.getIntrestSwap() == 1) {
                    cust.setxCoordinateTarget(0);
                    cust.setyCoordinateTarget(0);
                    cust.setIntrestSwap(cust.getIntrestSwap() - 1);
                    return;
                }

                if (cust.getIntrestSwap() == 0 && cust.getxCoordinate() == 0 && cust.getyCoordinate() == 0) {
                    cust.setInShop(false);
                }

                //CUSTOMER HAS EXAUSTED HIS TIME AT INTREST SPOT
                //CHOSE A NEW INTREST SPOT FOR HIM AT RANDOM
                int arr[] = {30, 60, 90, 120, 150, 180, 210, 240, 270, 300};
                int xVar = appDataManager.getStoresList().get(storeNum).getxVariables();
                int yVar = appDataManager.getStoresList().get(storeNum).getyVariables();
                int xPos = (int) (Math.random() * xVar);
                int yPos = (int) (Math.random() * yVar);

                //CUSTOMER HAS A NEW TARGET
                cust.setxCoordinateTarget(arr[xPos]);
                cust.setyCoordinateTarget(arr[yPos]);
                cust.setSmsSent(false);

                //SET THE AMOUNT OF TIME THE CUSTOMER SHOULD SPEND AT ONE SECTION
                this.setCustomerInterestTime(cust);

                //REDUCE THE NUMBER OF INTRESTS LEFT WITHT THE CUSTOMER
                cust.setIntrestSwap(cust.getIntrestSwap() - 1);
            }
        }
        //MOVE THE CUSTOMER TO THE NEW X AND Y
        cust.setxCoordinate(newX);
        cust.setyCoordinate(newY);

    }

    public void saveLocationData() {
        try {
            dataManager.saveCustomerLocationDataCSV(appDataManager, "E:\\data.csv");

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void printLocationData() {

        System.out.println("Serial | " + "xCoordinate | " + "yCoordinate | ");

        for (int i = 0; i < appDataManager.getStoresList().size(); i++) {
            for (int j = 0; j < appDataManager.getStoresList().get(i).getCustomers().size(); j++) {
                Customer d = appDataManager.getStoresList().get(i).getCustomers().get(j);
                if (d.isInShop()) {
                    System.out.println(d.getSerial() + " | " + d.getxCoordinate() + " | " + d.getyCoordinate());
                }

            }
        }

    }

    private void simulate() {

        boolean runOnce = true;
        boolean runSim = true;
        while (true) {
            runSim = false;
            for (int i = 0; i < appDataManager.getStoresList().size(); i++) {
                if (runOnce) {
                    this.generateInitialTargetTEMP(i, appDataManager.getStoresList().get(i).getxVariables(), appDataManager.getStoresList().get(i).getyVariables());
                }
                for (int n = 0; n < appDataManager.getStoresList().get(i).getCustomers().size(); n++) {

                    this.move(appDataManager.getStoresList().get(i).getCustomers().get(n), i);
                    if (appDataManager.getStoresList().get(i).getCustomers().get(n).isInShop()) {
                        runSim = true;
                    }
                }

            }
            this.saveLocationData();
            this.printLocationData();
            runOnce = false;
            if (!runSim) {
                break;
            }
        }

    }

    private void setCustomerNumberOfInterest(int minNumberInterest, int maxNumberInterest) {
        for (int i = 0; i < appDataManager.getStoresList().size(); i++) {
            for (int j = 0; j < appDataManager.getStoresList().get(i).getCustomers().size(); j++) {
                Random r = new Random();
                int Low = minNumberInterest;
                int High = maxNumberInterest;
                int Result = r.nextInt(High - Low) + Low;
                Customer d = appDataManager.getStoresList().get(i).getCustomers().get(j);
                d.setIntrestsLeft(Result);

            }
        }
    }

    private void setInitialCustomerInterestTime() {
        for (int i = 0; i < appDataManager.getStoresList().size(); i++) {
            for (int j = 0; j < appDataManager.getStoresList().get(i).getCustomers().size(); j++) {
                setCustomerInterestTime(appDataManager.getStoresList().get(i).getCustomers().get(j));
            }
        }
    }

    private void setCustomerInterestTime(Customer d) {

        Random r = new Random();
        int Low = customerDataManager.getMinInterestTimeForCustomer();
        int High = customerDataManager.getMaxInterestTimeForCustomer();
        int Result = r.nextInt(High - Low) + Low;
        d.setTimeSinceStationary(Result);

    }
}
