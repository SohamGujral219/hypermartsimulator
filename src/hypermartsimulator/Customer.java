package hypermartsimulator;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a customer, it's current location and it's desired
 * location.
 *
 * @author Soham Gujral
 */
public class Customer {

    //These are the basic required details for aquiring a customer  
    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty macAddress;
    private final StringProperty emailAddress;
    private final StringProperty mobileNumber;

    //SERIAL REPRESENTS A UNIQUE NUMBER ASSINGED TO EACH EMPLOYEE
    private IntegerProperty serial;

    //THESE ARE THE POSITION COORDINTES OF A CUSTOMER
    private double xCoordinate;
    private double yCoordinate;

    //THIS IS THE POSTION WHERE THE CUTOMER IS GOING TO GO. THIS COULD BE AN INTREST GROUP OR CHECKOUT(0,0)
    private double xCoordinateTarget;
    private double yCoordinateTarget;

    //THIS IS THE TIME SINCE THE CUSTOMER HAS BEEN IN ONE DEPARTMENT/SHOP
    private double timeSinceStationary;

    //THESE ARE THE NUMBER OF INTRESTS A CUSTOMER HAS
    private int intrestsLeft;

    //THIS TELLS US IF THE CUSTOMER IS PRESENT IN THE SHOP OR NOT
    private boolean inShop;

    //THIS IS A SMS COUNTER SHOOTER. WE WILL WAIT FOR THIS AMOUNT OF SECONDS TO SHOOT AN SMS
    private int shooterCounterSMS;
    //THIS TELLS US WEATHER AN SMS HAS BEEN SENT FOT THE CURRENT INTEREST
    private boolean smsSent;

    /**
     * THIS IS THE CONSTURCTOR FOR ALL CUSTOMERS ARE CREATED BY DEFAULT EVERYONE
     * WILL BE PLACES AT (0,0)
     *
     * @param firstName FIRST NAME OF CUST
     * @param lastName LAST NAME OF CUST
     * @param macAddress UNIQUE MAC ADDRESS OF CUSTOMER
     * @param emailAddress EMAIL ADDRESS OF CUST
     */
    public Customer(String firstName, String lastName, String macAddress, String emailAddress) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.macAddress = new SimpleStringProperty(macAddress);
        this.emailAddress = new SimpleStringProperty(emailAddress);
        this.xCoordinate = 0;
        this.yCoordinate = 0;
        this.mobileNumber = new SimpleStringProperty("+919717616965");
        inShop = true;
        serial = new SimpleIntegerProperty();
        shooterCounterSMS = 0;
        smsSent = false;
    }

    public void increamentShooter() {
        this.shooterCounterSMS += 1;
    }

    //FOLLOWING ARE ALL GETTERS AND
    public boolean isSmsSent() {
        return smsSent;
    }

    public void setSmsSent(boolean smsSent) {
        this.smsSent = smsSent;
    }

    public int getShooterCounterSMS() {
        return shooterCounterSMS;
    }

    public void setShooterCounterSMS(int counter) {
        this.shooterCounterSMS = counter;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public double getTimeSinceStationary() {
        return timeSinceStationary;
    }

    public void setTimeSinceStationary(double timeSinceStationary) {
        this.timeSinceStationary = timeSinceStationary;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public String getLastName() {
        return lastName.get();
    }

    public StringProperty getMacAddress() {
        return macAddress;
    }

    public String getEmailAddress() {
        return emailAddress.get();
    }

    public int getSerial() {
        return serial.get();
    }

    public void setSerial(int serial) {
        this.serial.set(serial);
    }

    public double getxCoordinateTarget() {
        return xCoordinateTarget;
    }

    public void setxCoordinateTarget(double xCoordinateTarget) {
        this.xCoordinateTarget = xCoordinateTarget;
    }

    public double getyCoordinateTarget() {
        return yCoordinateTarget;
    }

    public void setyCoordinateTarget(double yCoordinateTarget) {
        this.yCoordinateTarget = yCoordinateTarget;
    }

    public int getIntrestSwap() {
        return intrestsLeft;
    }

    public void setIntrestSwap(int intrestSwap) {
        this.intrestsLeft = intrestSwap;
    }

    public boolean isInShop() {
        return inShop;
    }

    public void setInShop(boolean inShop) {
        this.inShop = inShop;
    }

    public int getIntrestsLeft() {
        return intrestsLeft;
    }

    public void setIntrestsLeft(int intrestsLeft) {
        this.intrestsLeft = intrestsLeft;
    }

    public void setFirstName(String fName) {
        firstName.set(fName);
    }

    public void setLastName(String fName) {
        lastName.set(fName);
    }

    public void setEmailAddress(String fName) {
        emailAddress.set(fName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public StringProperty emailAddressProperty() {
        return emailAddress;
    }

    public StringProperty macAddressProperty() {
        return macAddress;
    }

    public StringProperty mobileNumberProperty() {
        return mobileNumber;
    }

    public IntegerProperty serialProperty() {
        return serial;
    }

    public StringProperty getMobileNumber() {
        return mobileNumber;
    }

    
}
