/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hypermartsimulator;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Soham
 */
public class StoreData {

    ObservableList<Store> storesList;
    private int uniqueStoreID;

    public StoreData() {
        storesList = FXCollections.observableArrayList();
        uniqueStoreID = 1;
    }

    public ObservableList<Store> getStoresList() {
        return storesList;
    }

    public void setStoresList(ObservableList<Store> storesList) {
        this.storesList = storesList;
    }

    public int getUniqueStoreID() {
        return uniqueStoreID;
    }

    public void setUniqueStoreID(int uniqueStoreID) {
        this.uniqueStoreID = uniqueStoreID;
    }

    void incrementStoreID() {
        uniqueStoreID += 1;
    }

}
