/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hypermartsimulator.fileIO;

//import hypermartsimulator.Customer;
//import hypermartsimulator.CustomerData;
//import hypermartsimulator.Store;
import hypermartsimulator.Customer;
import hypermartsimulator.CustomerData;
import hypermartsimulator.StoreData;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.json.JsonObject;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author Soham
 */
public class DataIO {

    private static final String JSON_CUSTOMER_ARRAY = "customerArray";
    private static final String JSON_SERIAL = "serial";
    private static final String JSON_FIRST_NAME = "first_name";
    private static final String JSON_LAST_NAME = "last_name";
    private static final String JSON_MAC_ADDRESS = "mac_address";
    private static final String JSON_EMAIL = "email";
    private static final String JSON_INTEREST = "interest";
    private static final String JSON_CUSTOMER_X = "xAxis";
    private static final String JSON_CUSTOMER_Y = "yAxis";

    private String outputFolderDir;
    private String inputFileDir;

    private int stationaryTime;
    private int smsThresholdTime;

    public void loadData(CustomerData data, String filePath) throws IOException {

        // CLEAR THE OLD DATA OUT
        CustomerData dataManager = (CustomerData) data;
        dataManager.Clear();

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        // NOW LOAD ALL CUSTOMERS
        JsonArray jsonCustomerArray = json.getJsonArray(JSON_CUSTOMER_ARRAY);
        for (int i = 0; i < jsonCustomerArray.size(); i++) {
            JsonObject jsonCusotmer = jsonCustomerArray.getJsonObject(i);
            String firstName = jsonCusotmer.getString(JSON_FIRST_NAME);
            String lastName = jsonCusotmer.getString(JSON_LAST_NAME);
            String macAddress = jsonCusotmer.getString(JSON_MAC_ADDRESS);
            String email = jsonCusotmer.getString(JSON_EMAIL);

            int serial = jsonCusotmer.getInt(JSON_SERIAL);

            Customer newCustomer = new Customer(firstName, lastName, macAddress, email);

            newCustomer.setSerial(serial);

            newCustomer.setxCoordinate(0);
            newCustomer.setyCoordinate(0);

            newCustomer.setTimeSinceStationary(this.stationaryTime);

            dataManager.addCustomer(newCustomer);
        }

    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    public void saveCustomerLocationDataCSV(StoreData appCustData, String locationDatajson) throws IOException {
        String csvFile = locationDatajson;
        FileWriter writer;

        writer = new FileWriter(csvFile, true);//REMOVE THE TRUE FOR A NEW FILE

        CSVUtils.writeLine(writer, Arrays.asList("UID", "X_Axis", "Y_Axis"));

        for (int i = 0; i < appCustData.getStoresList().size(); i++) {
            for (int j = 0; j < appCustData.getStoresList().get(i).getCustomers().size(); j++) {
                Customer d = appCustData.getStoresList().get(i).getCustomers().get(j);
                if (d.isInShop()) {
                    List<String> list = new ArrayList<>();
                    list.add(d.getSerial() + "");
                    list.add(d.getxCoordinate() + "");
                    list.add(d.getyCoordinate() + "");

                    CSVUtils.writeLine(writer, list);
                }
            }
        }

        writer.flush();
        writer.close();
    }

    public int getStationaryTime() {
        return stationaryTime;
    }

    public void setStationaryTime(int stationaryTime) {
        this.stationaryTime = stationaryTime;
    }

    public String getOutputFolderDir() {
        return outputFolderDir;
    }

    public void setOutputFolderDir(String outputFolderDir) {
        this.outputFolderDir = outputFolderDir;
    }

    public String getInputFileDir() {
        return inputFileDir;
    }

    public void setInputFileDir(String inputFileDir) {
        this.inputFileDir = inputFileDir;
    }

    public int getSmsThresholdTime() {
        return smsThresholdTime;
    }

    public void setSmsThresholdTime(int smsThresholdTime) {
        this.smsThresholdTime = smsThresholdTime;
    }

    
}
